# Free Remote Mysql



## Getting started

PlanetScale is a MySQL-compatible serverless database that brings you scale, performance, and reliability — without sacrificing developer experience.

With PlanetScale, you get the power of horizontal sharding, non-blocking schema changes, and many more powerful database features without the pain of implementing them.


Run PlanetScale, a MySQL-compatible serverless database on AWS. PlanetScale gives you the power, velocity and predictability you need to build apps that scale to the size of your vision. With cutting edge developer-first features like auto-scaling, non-blocking schema changes and real-time analytics you add power to your database layer without hiring a database expert. PlanetScale is evolving the database with features built to increase developer velocity like database branching, which allows your developers to work in staging branches before merging new schema changes to production, all without downtime. Their goal is to bring devops innovations to the database layer.

PlanetScale provides the most predictable platform for scaling your data by leveraging Vitess, the battle-proven and most reliable ACID-compliant MySQL orchestration layer on the planet. Connection pooling, query replication and Vitess under-the-hood mean you can reliably scale to thousands of queries per second if needed, all without leaving the cloud dashboard.


Let's deploy on https://render.com  

Result:  
curl https://freeremoteapi.onrender.com/products
